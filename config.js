const getEnv = () => ({
  mongoConnectionString: process.env.MONGO_CONNECTION_STRING,
  port: process.env.PORT
})

module.exports = {
  getEnv
}