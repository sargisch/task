const express = require("express");
const path = require("path");
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const cors = require('cors');

const database = require("./database");
const { getEnv } = require("./config");

const env = getEnv();
const { port } = env;

const app = express();

const bodyParser = require('body-parser')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(cors());

// Swagger definition
const swaggerDefinition = {
  info: {
    title: 'REST API for Interview Task',
    version: '1.0.0',
    description: 'This is the REST API for The Interview Task',
  },
  host: `localhost:${port}`,
  basePath: '/api',
};

// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: ['./docs/**/*.yaml'],
};
// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(options);

database.connect();

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

const clientRoutes = require('./routes/client');
const providerRoutes = require('./routes/provider');

app.use('/api/client', clientRoutes);
app.use('/api/provider', providerRoutes);

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});