const { body, param } = require('express-validator');

module.exports = {
  clientBodySchema: [
    body('email')
      .isEmail()
      .normalizeEmail()
      .withMessage('Invalid email'),
    body('name')
      .exists()
      .withMessage('Name is required')
      .isString()
      .withMessage('Name should be a string')
      .not().isEmpty()
      .withMessage('Name is required')
      .trim()
      .escape(),
    body('phone')
      .exists()
      .withMessage('Phone is required')
      .isNumeric()
      .withMessage('Phone should be a numeric value'),
    body('providers.*')
      .isMongoId()
      .withMessage('Provider should be an id'),
  ],
  clientDeleteSchema: [
    param('id')
      .exists()
      .withMessage('Id is required')
      .isMongoId()
      .withMessage('Id should be a mongo id')
  ]
}
