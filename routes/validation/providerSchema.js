const { body, param } = require('express-validator');

module.exports = {
  providerBodySchema: [
    body('name')
      .isString()
      .withMessage('Name should be a string')
      .exists()
      .withMessage('Name is required')
      .notEmpty()
      .withMessage('Name is required')
  ],
  providerDeleteSchema: [
    param('id')
      .exists()
      .withMessage('Id is required')
      .isMongoId()
      .withMessage('Id should be a mongo id')
  ]
}
