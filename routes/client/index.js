const express = require('express');
const ClientApi = require('./ClientApi');
const router = express.Router();
const { clientBodySchema, clientDeleteSchema } = require('../validation/clientSchema');
const validate = require('../validation/validate');

router.get('/', (req, res) => {
  const api = new ClientApi(req, res);
  return api.get();
});

router.post('/', clientBodySchema, validate, (req, res) => {
  const api = new ClientApi(req, res);
  return api.create();
});

router.put('/:id', clientBodySchema, validate, (req, res) => {
  const api = new ClientApi(req, res);
  return api.edit();
});

router.delete('/:id', clientDeleteSchema, validate, (req, res) => {
  const api = new ClientApi(req, res);
  return api.delete();
});

module.exports = router;