const ClientModel = require("../../schemas/client");
const Api = require("../../Api");
const ObjectId = require('mongoose').Types.ObjectId
class ClientApi extends Api {
  async get() {
    try {
      const query = {};
      if (this.req.query.searchField && this.req.query.searchTerm) {
        query[this.req.query.searchField] = {
          $regex: new RegExp(this.req.query.searchTerm)
        };
      }
      const clients = await ClientModel
        .find(query)
        .populate('providers')
        .select('-__v')
      return this.response({ clients });
    } catch (e) {
      this.responseError(500, e.message);
    }
  }

  async create() {
    const clientData = this.req.body;
    try {
      const client = new ClientModel(clientData);
      const newClient = await client.save();
      const newClientPopulated = await newClient.populate('providers').execPopulate();
      return this.response({ client: newClientPopulated });
    } catch (e) {
      this.responseError(500, e.message);
    }
  }

  async edit() {
    const clientId = this.req.params.id;
    const clientData = this.req.body;
    try {
      const client = await ClientModel.findOne({ _id: ObjectId(clientId) });
      if (client === null) {
        this.responseError(400, `Client ${clientId} does not exist`);
        return;
      }
      const updatedClient = await ClientModel.findOneAndUpdate({
        _id: ObjectId(clientId)
      }, { $set: clientData }, { new: true }).populate('providers')
      return this.response({ client: updatedClient });
    } catch (e) {
      this.responseError(500, e.message);
    }
  }

  async delete() {
    const clientId = this.req.params.id;
    try {
      const result = await ClientModel.deleteOne({ _id: Object(clientId) });
      if (result.deletedCount !== 1) {
        return this.responseError(400, `The client ${clientId} is not deleted`);
      }
      return this.response({ deletedClientId: clientId });
    } catch (e) {
      this.responseError(500, e.message);
    }
  }
}

module.exports = ClientApi