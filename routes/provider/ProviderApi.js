const ProviderModel = require("../../schemas/provider");
const Api = require("../../Api");
const ObjectId = require('mongoose').Types.ObjectId
class ProviderApi extends Api {
  async get() {
    const id = this.req.query.providerId
    try {
      const query = {};
      if (id) {
        query._id = ObjectId(id);
      }
      if (this.req.query.searchField && this.req.query.searchTerm) {
        query[this.req.query.searchField] = {
          $regex: new RegExp(this.req.query.searchTerm)
        };
      }
      const providers = await ProviderModel.find(query).select('-__v')
      return this.response({ providers });
    } catch (e) {
      this.responseError(500, e.message);
    }
  }
  async create() {
    const providerData = this.req.body;
    try {
      const provider = new ProviderModel(providerData);
      const newProvider = await provider.save();
      return this.response({ provider: newProvider });
    } catch (e) {
      this.responseError(500, e.message);
    }
  }
  async edit() {
    const providerId = this.req.params.id
    const providerData = this.req.body;
    try {
      const provider = await ProviderModel.findOne({ _id: ObjectId(providerId) });
      if (provider === null) {
        this.responseError(400, `Provider ${providerId} does not exist`);
        return;
      }
      const updatedProvider = await ProviderModel.findOneAndUpdate({
        _id: ObjectId(providerId)
      }, { $set: providerData }, { new: true });
      return this.response({ provider: updatedProvider });
    } catch (e) {
      this.responseError(500, e.message);
    }
  }

  async delete(_id) {
    const providerId = this.req.params.id;
    try {
      const result = await ProviderModel.deleteOne({ _id: Object(providerId) });
      if (result.deletedCount !== 1) {
        return this.responseError(400, `The provider ${providerId} is not deleted`);
      }
      return this.response({ deletedProviderId: providerId });
    } catch (e) {
      this.responseError(500, e.message);
    }
  }
}

module.exports = ProviderApi