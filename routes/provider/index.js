const express = require('express');
const ProviderApi = require('./ProviderApi');
const router = express.Router();
const { providerBodySchema, providerDeleteSchema } = require('../validation/providerSchema');
const validate = require('../validation/validate');

router.get('/', (req, res) => {
  const api = new ProviderApi(req, res);
  return api.get();
});

router.post('/', providerBodySchema, validate, (req, res) => {
  const api = new ProviderApi(req, res);
  return api.create();
});

router.put('/:id', providerBodySchema, validate, (req, res) => {
  const api = new ProviderApi(req, res);
  return api.edit();
});

router.delete('/:id', providerDeleteSchema, validate, (req, res) => {
  const api = new ProviderApi(req, res);
  return api.delete();
});

module.exports = router;