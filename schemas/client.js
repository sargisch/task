var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var clientSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  providers: {
    type: [{
      type: mongoose.Types.ObjectId,
      ref: 'Provider'
    }],
    default: [],
  }
});

module.exports = mongoose.model('Client', clientSchema);