var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var providerSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
});


module.exports = mongoose.model('Provider', providerSchema);