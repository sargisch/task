class Api {
  constructor(req, res) {
    this.req = req;
    this.res = res;
  }
  response(data, message = '') {
    return this.res.json({
      ok: true,
      data,
      message
    })
  }
  responseError(statusCode, message='something went wrong'){
    return this.res.status(statusCode).json({
      ok: false,
      message
    })
  }
}

module.exports = Api;