const mongoose = require('mongoose');
const { getEnv } = require('./config');

mongoose.connection.on('error', (err) => {
  console.log(`MongoDB connection error: ${err}`);
  process.exit(-1);
});

const { mongoConnectionString } = getEnv();

module.exports = {
  connect: () => {
    mongoose
      .connect(mongoConnectionString, {
        useCreateIndex: true,
        keepAlive: 1,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      })
      .then(() => console.log('mongoDB connected...'));
    return mongoose.connection;
  }
};
