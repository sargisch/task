import env from './env';


class Request {
  constructor(endpoint) {
    this.endpoint = endpoint
  }
  get(queryString) {
    return fetch(`${env.apiUrl}/${this.endpoint}?${queryString || ''}`)
      .then(res => res.json())
  }
  post(data) {
    return fetch(`${env.apiUrl}/${this.endpoint}`, {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
  }
  put(data) {
    return fetch(`${env.apiUrl}/${this.endpoint}/${data._id}`, {
      method: 'put',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
  }
  delete(id) {
    return fetch(`${env.apiUrl}/${this.endpoint}/${id}`, {
      method: 'delete',
    })
      .then(res => res.json())
  }
}

export default Request;