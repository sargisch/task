
let timeoutId = null;
export default {
  data() {
    return {
      formModalOpened: false,
      deleteModalOpened: false,
      searchTerm: '',
      selectedSearchField: 'name'
    }
  },
  created() {
    this.request.get().then((res) => {
      this.setList(res);
    });
  },
  watch: {
    formModalOpened(newValue) {
      if (!newValue) {
        this.formData = this.buildInitialData();
      }
    },
    searchTerm() {
      if (timeoutId)
        clearTimeout(timeoutId);
      timeoutId = setTimeout(() => {
        this.request
          .get(`searchField=${this.selectedSearchField}&searchTerm=${this.searchTerm}`)
          .then(res => {
            this.setList(res);
          })
      }, 1000)
    },
    selectedSearchField() {
      if (this.searchTerm) {
        this.request.get(`searchField=${this.selectedSearchField}&searchTerm=${this.searchTerm}`)
          .then(res => {
            this.setList(res);
          })
      }
    },
  },
  methods: {
    submit() {
      if (this.formData._id) this.edit();
      else this.create();
    },
    toggleFormModal() {
      this.formModalOpened = !this.formModalOpened;
    },
    create() {
      const keys = Object.keys(this.formData);
      const formData = {};
      keys.forEach(key => {
        if (key !== '_id') formData[key] = this.formData[key];
      })
      this.request.post(formData).then((res) => {
        this.handleCreate(res)
        this.toggleFormModal();
      });
    },
    edit() {
      this.request.put(this.formData).then((res) => {
        this.handleEdit(res);
        this.toggleFormModal();
      });
    },
    onEditClick(rowData) {
      this.formData = Object.assign({}, rowData);
      this.toggleFormModal();
    },
    openDeleteModal({ _id }) {
      this.deleteModalOpened = false;
      this.deleteId = _id;
      this.$root.$emit("bv::show::modal", "delete-modal");
    },
    closeDeleteModal() {
      this.deleteId = "";
      this.deleteModalOpened = false;
    },
    deleteRecord() {
      this.request.delete(this.deleteId).then((res) => {
        this.handleDelete(res);
        this.closeDeleteModal();
      });
    },
  },
};