import Vue from 'vue'
import vSelect from 'vue-select'
import VueRouter from 'vue-router'
import 'vue-select/dist/vue-select.css';
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Clients from './components/Clients.vue'
import Providers from './components/Providers.vue'

Vue.config.productionTip = false

const routes = [
  { path: '/clients', component: Clients },
  { path: '/providers', component: Providers },
  { path: '/providers/:id', component: Providers }
]

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueRouter)
Vue.component('v-select', vSelect)


const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
