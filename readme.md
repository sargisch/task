# Interview Task #
  * This is a task project for interview
  * The project uses mongo, express, vue and bootstrap
  * All Apis documented by swagger

# server

## Project setup

## /docs
  .yaml swagger files for documentation
  http://host:port/docs 

## .env
  create env file with
  
  * MONGO_CONNECTION_STRING=mongodb://127.0.0.1:27017/interview_task
  *  PORT=4000
  

```
npm install
```
### Start server
```
npm run start
```

# client

## Project setup

## cd client

## .env
  create env file with
  
  * VUE_APP_API_URL=http://localhost:4000/api

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### routes

```
  /clients - clients list 
  /providers - providers list 

```